#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2024/3/12 16:23
# @Author  : fu
# @Site    : 
# @File    : testlogin.py
# @Project : 20240312
# @Software: PyCharm
# @脚本用途

l = [3, 2, 4, 5, 6, 8, 1, 7, 9]
n = len(l)

for i in range(n - 1):
    for j in range(0, n - i - 1):
        if l[j] > l[j + 1]:
            l[j], l[j + 1] = l[j + 1], l[j]

print(l)
